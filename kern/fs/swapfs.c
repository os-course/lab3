#include <swap.h>
#include <swapfs.h>
#include <mmu.h>
#include <fs.h>
#include <ide.h>
#include <pmm.h>
#include <assert.h>

void
swapfs_init(void) {
    static_assert((PGSIZE % SECTSIZE) == 0); //检查页大小是否是扇区大小的整数倍。
    if (!ide_device_valid(SWAP_DEV_NO)) { //检查交换设备的有效性
        panic("swap fs isn't available.\n");
    }
    max_swap_offset = ide_device_size(SWAP_DEV_NO) / (PGSIZE / SECTSIZE); //交换设备的最大偏移量，即交换设备的扇区数除以页的扇区数。
}

//swapfs_read 从交换设备读取数据到page对应的物理页中。
//swap_offset(entry) * PAGE_NSECT 起始扇区号
//将一个Page结构体指针page转换为对应的内核虚拟地址。
int
swapfs_read(swap_entry_t entry, struct Page *page) {
    return ide_read_secs(SWAP_DEV_NO, swap_offset(entry) * PAGE_NSECT, page2kva(page), PAGE_NSECT);
}

int
swapfs_write(swap_entry_t entry, struct Page *page) {
    return ide_write_secs(SWAP_DEV_NO, swap_offset(entry) * PAGE_NSECT, page2kva(page), PAGE_NSECT);
}

