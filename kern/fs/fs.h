#ifndef __KERN_FS_FS_H__
#define __KERN_FS_FS_H__

#include <mmu.h>

#define SECTSIZE            512 //扇区大小为512字节
#define PAGE_NSECT          (PGSIZE / SECTSIZE) //页中包含的扇区数目

#define SWAP_DEV_NO         1 //交换设备的编号为1

#endif /* !__KERN_FS_FS_H__ */

