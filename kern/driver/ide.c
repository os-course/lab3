#include <assert.h>
#include <defs.h>
#include <fs.h>
#include <ide.h>
#include <stdio.h>
#include <string.h>
#include <trap.h>
#include <riscv.h>

void ide_init(void) {}

#define MAX_IDE 2
#define MAX_DISK_NSECS 56 //磁盘的扇区数
static char ide[MAX_DISK_NSECS * SECTSIZE]; //模拟磁盘的存储空间。SECTSIZE表示扇区的大小

//检查设备编号是否有效
bool ide_device_valid(unsigned short ideno) { return ideno < MAX_IDE; }

//获取指定IDE设备的大小
size_t ide_device_size(unsigned short ideno) { return MAX_DISK_NSECS; }

//从指定IDE设备的指定扇区读取数据
//ideno: 设备编号
//secno: 扇区编号
//dst: 目标缓冲区
//nsecs: 扇区数
int ide_read_secs(unsigned short ideno, uint32_t secno, void *dst,
                  size_t nsecs) {
    int iobase = secno * SECTSIZE;
    memcpy(dst, &ide[iobase], nsecs * SECTSIZE);
    return 0;
}

//将数据写入指定IDE设备的指定扇区
int ide_write_secs(unsigned short ideno, uint32_t secno, const void *src,
                   size_t nsecs) {
    int iobase = secno * SECTSIZE;
    memcpy(&ide[iobase], src, nsecs * SECTSIZE);
    return 0;
}
